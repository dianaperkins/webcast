$(function(){

  // Countdown Timer
  // January 26, 2018 3:00PM EST
	$('#clock').countdown('1516996800000', function(event) {

    //s plitting the numbers into two digits. Adding a 0 before single digits. 
    var daysToDisplay = event.offset.totalDays
    if (daysToDisplay < 10) {
      $("#days-0").text("0")
      $("#days-1").text(daysToDisplay)
    } else {
      var daysString = daysToDisplay.toString()
      $("#days-0").text(daysString[0])
      $("#days-1").text(daysString[1])
    }

    var hoursToDisplay = event.offset.hours
    if (hoursToDisplay < 10) {
      $("#hours-0").text("0")
      $("#hours-1").text(hoursToDisplay)
    } else {
      var hoursString = hoursToDisplay.toString()
      $("#hours-0").text(hoursString[0])
      $("#hours-1").text(hoursString[1])
    }

    var minutesToDisplay = event.offset.minutes
    if (minutesToDisplay < 10) {
      $("#minutes-0").text("0")
      $("#minutes-1").text(minutesToDisplay)
    } else {
      var minutesString = minutesToDisplay.toString()
      $("#minutes-0").text(minutesString[0])
      $("#minutes-1").text(minutesString[1])
    }

    var secondsToDisplay = event.offset.seconds
    if (secondsToDisplay < 10) {
      $("#seconds-0").text("0")
      $("#seconds-1").text(secondsToDisplay)
    } else {
      var secondsString = secondsToDisplay.toString()
      $("#seconds-0").text(secondsString[0])
      $("#seconds-1").text(secondsString[1])
    }
	});

  // Form Validation
  $("#email-form").validate({
    rules: {
      // simple rule, converted to {required:true}
      firstname: "required",
      lastname: "required",
      // compound rule
      email: {
        required: true,
        email: true
      }
    },
    messages: {
      firstname: "Whoops! We need your first name.",
      lastname: "Oops, we need your last name, too.",
      email: {
        required: "We need your email address to save your spot.",
        email: "Your email address must be in the format of name@domain.com"
      }
    }
  });

    //all heading 1s
    $('h1').widowFix();
    $('h2').widowFix();
});